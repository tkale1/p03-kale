//
//  BreakoutViewController.m
//  p03-Kale
//
//  Created by Tanmay Kale on 2/15/16.
//  Copyright © 2016 tkale1. All rights reserved.
//

#import "BreakoutViewController.h"

@interface BreakoutViewController ()

@end

@implementation BreakoutViewController

@synthesize paddle, ball;
@synthesize BottonLine;
@synthesize timer;
@synthesize bricks;
int currentScore=0;
@synthesize ScoreLabel;
@synthesize Score;
@synthesize Home;
- (IBAction)HomeClicked:(id)sender {
    
     [timer invalidate];
}

-(void)playAudio
{
    
    AudioServicesPlaySystemSound(PlaySoundID);
}
-(void)GameOverSound
{
    
    AudioServicesPlaySystemSound(PlaySoundID1);
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSURL *SoundURL= [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Sound" ofType:@"mp3"]];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)SoundURL,&PlaySoundID);
    
    NSURL *SoundURL1= [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"gameOverSound" ofType:@"wav"]];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)SoundURL1,&PlaySoundID1);

    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"WallBackground.jpg"]];
    //self.view.backgroundColor = [UIColor blackColor];
    // Do any additional setup after loading the view.
    
    paddle = [[UIView alloc] initWithFrame:CGRectMake(100, 700, 80, 17)];
    [self.view addSubview:paddle];
    [paddle setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Bricksss.jpg"]]];
    paddle.layer.borderWidth = 2;
    paddle.layer.borderColor = [UIColor blackColor].CGColor;
    
    //ball = [[UIView alloc] initWithFrame:CGRectMake(100, 100, 20, 20)];
    //[self.view addSubview:ball];
    //[ball setBackgroundColor:[UIColor redColor]];

    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    CGFloat screenHeight = screenSize.height;
    
    ball = [[UIView alloc]initWithFrame:CGRectMake(screenWidth/2, screenHeight/2, 15, 15)];
    ball.layer.borderWidth = 1.0f;
    ball.layer.borderColor = [UIColor blackColor].CGColor;
    ball.layer.backgroundColor =[UIColor redColor].CGColor;
    ball.layer.cornerRadius = ball.frame.size.width/2.0;
    ball.layer.masksToBounds = YES;
    [self.view addSubview: ball];
    

    BottonLine = [[UIView alloc] initWithFrame:CGRectMake(0, screenHeight-25, screenWidth, screenHeight)];
    [BottonLine setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bottom.jpg"]]];
    [self.view addSubview: BottonLine];
    
    Score = [[UILabel alloc] initWithFrame:CGRectMake(screenWidth-180, 30, 80, 15)];
    Score.font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:(22.0)];
    Score.text=@"Score : ";
    Score.textAlignment =  UITextAlignmentCenter;
    [self.view addSubview: Score];
    
    ScoreLabel = [[UILabel alloc] initWithFrame:CGRectMake(screenWidth-100, 30, 50, 15)];
    ScoreLabel.text=@"0";
    ScoreLabel.textAlignment =  UITextAlignmentCenter;
    ScoreLabel.font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:(22.0)];
    [self.view addSubview: ScoreLabel];
    
//    Home = [[UIButton alloc] initWithFrame:CGRectMake(20, 30, 100, 15)];
//    [Home setTitle:@"Home" forState:UIControlStateNormal];
//    Home.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:(22.0)];
//    [Home setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    [self.view addSubview: Home];
    
    bricks = [NSMutableArray array];
    
    int y = 80,x=20;
    for(int k=0; k<7; k ++){
        x=10;
        for (int i=0;i<10;i++){
            UILabel *brick;
           // int x = (screenWidth/10)*i;
            
            brick= [[UILabel alloc] initWithFrame:CGRectMake(x, y, screenWidth/10, 15)];
            [brick setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"brickImage.jpg"]]];
            brick.layer.borderColor = [UIColor whiteColor].CGColor;
            brick.layer.borderWidth = 2;
            [self.view addSubview:brick];
            [bricks addObject:brick];
            x=x+39;
        }
        y = y + 15;
    }
    
    dx = 10;
    dy = 10;
    
    
    
    
    timer = [NSTimer scheduledTimerWithTimeInterval:.05	target:self selector:@selector(timerEvent:) userInfo:nil repeats:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    for (UITouch *t in touches)
    {
        CGPoint p = [t locationInView:self.view];
        p.y=707;
        [paddle setCenter:p];
        
    }
}

-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self touchesBegan:touches withEvent:event];
}


-(void)timerEvent:(id)sender
{
    CGRect bounds = [self.view bounds];
    
    // NSLog(@"Timer event.");
    CGPoint p = [ball center];
    
    if ((p.x + dx) < 0)
        dx = -dx;
    
    if ((p.y + dy) < 0)
        dy = -dy;
    
    if ((p.x + dx) > bounds.size.width)
        dx = -dx;
    
    if ((p.y + dy) > bounds.size.height)
        dy = -dy;
    
    
    p.x += dx;
    p.y += dy;
    [ball setCenter:p];
    
    // Now check to see if we intersect with paddle.  If the movement
    // has placed the ball inside the paddle, we reverse that motion
    // in the Y direction.
    if (CGRectIntersectsRect([ball frame], [paddle frame]))
    {
        dy = -dy;
        p.y += 3*dy;
        [ball setCenter:p];
        [self playAudio];
    }
    
    if (CGRectIntersectsRect([ball frame], [BottonLine frame ]))
    {
        [self GameOverSound];
        [timer invalidate];
        [self showOverMessage];
    }
    
    NSUInteger arraySize = [bricks count];
    for(int j=0; j<arraySize; j++){
        UILabel *x =[bricks objectAtIndex:j];
        if (CGRectIntersectsRect([ball frame], [x frame]))
        {
            if(!x.isHidden){
                //YourClass found!!
                [self playAudio];
                dy = -dy;
                x.hidden = YES;
                currentScore+=10;
                ScoreLabel.text= [NSString stringWithFormat:@"%d", currentScore];
                
            }
        }
        //if(!brick1.isHidden){
        // dy = -dy;
        //  brick1.hidden = YES;
        //}
    }
    
    int bricksCount = 0;
    for(int k=0;k<arraySize;k++){
        UILabel *x =[bricks objectAtIndex:k];
        if(x.isHidden){
            bricksCount++;
        }
    }
    if(bricksCount == arraySize)
    {
        [timer invalidate];
        [self showWinMessage];
    }
}

- (void) showWinMessage {
    _winLose.text=@"YOU WIN..!!";
    _winLose.hidden=NO;
    
    
    }

- (void) showOverMessage {
    _winLose.text=@"SORRY YOU LOST..!!";
    _winLose.hidden=NO;
    
    
}



@end
