//
//  BreakoutViewController.h
//  p03-Kale
//
//  Created by Tanmay Kale on 2/15/16.
//  Copyright © 2016 tkale1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>

@interface BreakoutViewController : UIViewController
{
    float dx, dy;  // Ball motion
    
    SystemSoundID PlaySoundID;
    SystemSoundID PlaySoundID1;
}
@property (strong, nonatomic) IBOutlet UIButton *homeButton;

@property (nonatomic, strong) UIView *paddle;
@property (nonatomic, strong) UIView *ball;
@property (nonatomic, strong) UIView *BottonLine;
@property (nonatomic, strong) NSTimer *timer;

@property (nonatomic, strong) NSMutableArray *bricks;
@property (strong, nonatomic) IBOutlet UILabel *ScoreLabel;
@property (strong, nonatomic) IBOutlet UILabel *Score;
@property (strong, nonatomic) IBOutlet UIButton *Home;
@property (strong, nonatomic) IBOutlet UILabel *winLose;

extern int currentScore;
@end
