BRICK BREAKER: - It's a simple game in which you have to destroy all the bricks by not letting the ball go below the user controller tray and win the level when all the bricks are destroyed. 

This game is Developed in purely Objective-C using Xcode for iOS

![Screen Shot 1.png](https://bitbucket.org/repo/XoG98E/images/862704295-Screen%20Shot%201.png)

![Screen Shot 2.png](https://bitbucket.org/repo/XoG98E/images/1435737989-Screen%20Shot%202.png)

![Screen Shot 3.png](https://bitbucket.org/repo/XoG98E/images/676526854-Screen%20Shot%203.png)

![Screen Shot 4.png](https://bitbucket.org/repo/XoG98E/images/2752302036-Screen%20Shot%204.png)